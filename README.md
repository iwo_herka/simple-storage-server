Requirements:

- Python 3.4.3
- Django Framework 1.8.5

Setup:

- python manage.py migrate

How to run server:

- python manage.py runserver 0.0.0.0:8080