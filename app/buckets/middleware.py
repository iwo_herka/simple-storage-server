# -*- coding: utf-8 -*-
from django.http import HttpResponse

class ParsingAuthHeaderException(Exception):
    pass

class BadLoginOrPasswordException(Exception):
    pass

class ExceptionMiddleware(object):
    def process_exception(self, request, exception):
        if isinstance(exception, ParsingAuthHeaderException):
            return HttpResponse('Parsing Authorization header failed', status=400)
        elif isinstance(exception, BadLoginOrPasswordException):
            return HttpResponse('Bad login and/or password', status=401)
        else:
            return HttpResponse(exception, status=500)

