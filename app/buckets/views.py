# -*- coding: utf-8 -*-
import json
import base64
import os
import platform
import shutil
import codecs

from django.db import transaction
from django.views.generic import View
from django.http import HttpResponse

from ..common.views import AuthorizationMixin
from ..accounts.models import Account
from .middleware import ParsingAuthHeaderException, BadLoginOrPasswordException

class ImATeapot(View):
    def get(self, request):
        return HttpResponse('I\'m a Teapot!', status=418)

class BucketManagerView(AuthorizationMixin, View):
    def __init__(self, *args, **kwargs):
        if platform.system() is 'Windows':
            self.separator = '\\'
        else:
            self.separator = '/'

        # set user space directory path
        self.users_dir = os.path.dirname(os.path.abspath(__file__)) + self.separator + 'user_space'

        # initialize user space if needed
        if not os.path.exists(self.users_dir):
            os.makedirs(self.users_dir)

        super(BucketManagerView, self).__init__(*args, **kwargs)

    def get(self, request, path):
        user_dir = self.users_dir + self.separator + self.login
        if not os.path.exists(user_dir):
            os.makedirs(user_dir)

        path = path.split('/')
        filename = path.pop()
        file_path = user_dir

        for bucket in path:
            file_path += self.separator + bucket

            if not os.path.exists(file_path):
                return HttpResponse('Specified path is invalid', status=400)

        file_path = file_path + self.separator + filename
        
        if os.path.isfile(file_path):
            f = open(file_path, 'r')
            contents = f.read()
            f.close()

            return HttpResponse(contents, status=200)
        else:
            return HttpResponse('File does not exist', status=404)

    def post(self, request, path):
        user_dir = self.users_dir + self.separator + self.login
        if not os.path.exists(user_dir):
            os.makedirs(user_dir)

        path = path.split('/')
        something_new = False

        try:
            for bucket in path:
                user_dir += self.separator + bucket

                if not os.path.exists(user_dir):
                    something_new = True
                    os.makedirs(user_dir)
        except IOError:
            return HttpResponse('Creating directory failed', status=500)

        if something_new:
            return HttpResponse('Directory tree created', status=201)
        else:
            return HttpResponse('Directory tree already exists', status=200)

    def put(self, request, path):
        user_dir = self.users_dir + self.separator + self.login
        if not os.path.exists(user_dir):
            os.makedirs(user_dir)

        path = path.split('/')
        filename = path.pop()
        file_path = user_dir

        for bucket in path:
            file_path += self.separator + bucket

            if not os.path.exists(file_path):
                return HttpResponse('Specified path is invalid', status=400)

        modified = False
        if os.path.isfile(file_path + self.separator + filename):
            modified = True

        f = open(file_path + self.separator + filename, 'wb')
        f.write(request.body)
        f.close()

        if not modified:
            return HttpResponse('File created', status=201)      
        else:
            return HttpResponse('File updated', status=200)

    def delete(self, request, path):
        user_dir = self.users_dir + self.separator + self.login
        if not os.path.exists(user_dir):
            os.makedirs(user_dir)

        path = path.split('/')
        target = path.pop()
        file_path = user_dir

        for bucket in path:
            file_path += self.separator + bucket

            if not os.path.exists(file_path):
                return HttpResponse('Specified path is invalid', status=400)

        target_path = file_path + self.separator + target
        if not os.path.exists(target_path):
            return HttpResponse('Specified target doesn\'t not exist', status=404)

        try:      
            shutil.rmtree(target_path)
            return HttpResponse('Directory deleted', status=200)
        except NotADirectoryError:
            try:
                os.remove(target_path)
                return HttpResponse('File deleted', status=200)
            except Error:
                return HttpResponse('Error occurred during operation', status=500)
        
class HomeView(View):
    def __init__(self, *args, **kwargs):
        self.allowed_methods = ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS']
        super(HomeView, self).__init__(*args, **kwargs)

    def options(self, request):
        response = HttpResponse()
        response['Allow'] = ','.join(self.allowed_methods)
        return response