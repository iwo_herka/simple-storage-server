# -*- coding: utf-8 -*-
import json
import base64

from django.http import HttpResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt

from ..accounts.models import Account
from ..common.views import AuthorizationMixin

class AccountCreateView(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(AccountCreateView, self).dispatch(request, *args, **kwargs)

    def __init__(self, *args, **kwargs):
        self.allowed_methods = ['POST', 'OPTIONS']
        super(AccountCreateView, self).__init__(*args, **kwargs)

    def post(self, request):
        # check if content is JSON
        if request.META['CONTENT_TYPE'] != 'application/json':
            return HttpResponse('Supported format is JSON', status=406)

        try:
            json_obj = json.loads(request.body.decode('utf-8'))
        except ValueError as e:
            return HttpResponse('Decoding JSON has failed, reason: ' + str(e), status=400)

        login = json_obj['login']
        password = json_obj['password']

        # validate login and key
        if len(json_obj['password']) < 8:
            return HttpResponse('Password must be at least 8 characters long', status=406)
        elif Account.objects.filter(login=login).count() > 0:
            return HttpResponse('Login is already in use', status=406)

        # update database
        Account.objects.create(login=login, password=password)

        return HttpResponse('Account was created successfully', status=201)

    def options(self, request):
        data = {}
        data['login'] = 'Unique login of your chosing'
        data['password'] = 'User password'
        json_data = json.dumps(data)

        response = HttpResponse(
            json_data, 
            status=200
        )
        response['Allow'] = ','.join(self.allowed_methods)

        return response

class AccountDeleteView(AuthorizationMixin, View):
    def __init__(self, *args, **kwargs):
        self.allowed_methods = ['POST', 'OPTIONS']
        super(AccountDeleteView, self).__init__(*args, **kwargs)

    def post(self, request, login):
        if self.login != login:
            return HttpResponse('Login specified in header does not match the one specified in body', status=401)

        Account.objects.get(login=login).delete()
        return HttpResponse('Account was deleted', status=200)

    def options(self, request):
        data = {}
        data['login'] = 'User login to be deleted'
        data['password'] = 'User password'
        json_data = json.dumps(data)

        response = HttpResponse(
            json_data, 
            status=200
        )
        response['Allow'] = ','.join(self.allowed_methods)

        return response