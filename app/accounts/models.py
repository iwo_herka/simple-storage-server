# -*- coding: utf-8 -*-
from django.db import models

class Account(models.Model):
    login = models.CharField(max_length=24, unique=True)
    password = models.CharField(max_length=100)
