# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.http import HttpResponse
from django.contrib.auth.models import User

from .buckets.views import *
from .accounts.views import *

urlpatterns = [
    url(r'\*', ImATeapot.as_view(), name='im_a_teapot'),
    url(r'^$', HomeView.as_view(), name='home'),

    # user management
    url(r'^account/create/$', AccountCreateView.as_view(), name='create_account'),
    url(r'^account/delete/(?P<login>\w+)/$', AccountDeleteView.as_view(), name='delete_account'),

    # file management
    url(r'^bucket/(?P<path>.*)/$', BucketManagerView.as_view(), name='bucket_manager'),
]