# -*- coding: utf-8 -*-
import base64

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from ..accounts.models import Account
from ..buckets.middleware import ParsingAuthHeaderException, BadLoginOrPasswordException

class AuthorizationMixin(object):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        try:
            credentials = base64.b64decode(request.META['HTTP_AUTHORIZATION'].split(' ')[1]).decode('utf-8').split(':')
        except ValueError:
            raise ParsingAuthHeaderException

        if Account.objects.filter(login=credentials[0], password=credentials[1]).count() != 1:
            raise BadLoginOrPasswordException

        self.login = credentials[0]
        self.password = credentials[1]
        return super(AuthorizationMixin, self).dispatch(request, *args, **kwargs)